$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "activity_digest/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "activity_digest"
  s.version     = ActivityDigest::VERSION
  s.authors     = ["Kevin Mehlbrech"]
  s.email       = ["mynock51@gmail.com"]
  s.homepage    = "https://bitbucket.org/mynock/activity_digest"
  s.summary     = "A Rails plugin for streamlining scheduled activity digests."
  s.description = "TODO: Description of ActivityDigest."
  s.license     = "MIT"

  s.files         = `git ls-files`.split($/)
  s.executables   = s.files.grep(%r{^bin/}) { |f| File.basename(f) }
  s.test_files    = s.files.grep(%r{^(test|spec|features)/})
  s.require_paths = ['lib']


  s.add_dependency "rails", "~> 4.1.6"

  s.add_development_dependency 'rspec-rails', '~> 2.14.0'
  s.add_development_dependency 'sqlite3',     '~> 1.3.8'
  s.add_development_dependency 'byebug'

end
